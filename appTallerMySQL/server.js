//Inicialización de variables
// importamos express y mongoose e indicamos el puerto por el que será accesible la app.
var express = require('express');
var morgan         = require('morgan');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var app = express();

//Parte MySQL
var mysql = require('mysql');
var connectionpool = mysql.createPool({
    host    :   'localhost',
    user    :   'root',
    password:   '',
    database:   'bddMySQL'
});

//Configuración del servidor (Actualizado a Express 4.*).
    app.use(express.static(__dirname + '/angular'));    //Creamos el midleware estático.
    app.use(morgan('dev'));                     //Activamos el log de los request.
    app.use(bodyParser());  //Extrae la información del HTML en las llamadas POST.
    app.use(methodOverride());                  //Simula DELETE y PUT.

//Cargamos los endpoints
require('./app/routes.js')(app);

//Ponemos a escuchar al servidor en el puerto designado.
app.listen(8083);
console.log('App por el puerto 8083.');