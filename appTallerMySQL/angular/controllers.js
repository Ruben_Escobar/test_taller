"use strict";   //Uso estricto de javaScript
//Definimos el controlador de la vista.
app.controller('insCtr', function ($scope, ngTableParams, $timeout, $interval, $http){
    
/*----------------------------------------------------------------------------------------------*/
/*------------------------------------ Variables del modelo ------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
    
    $scope.cont = 1;
    
    $scope.grupoSel = "";
    $scope.fasesGrupoSel = "";
    $scope.tuNombre = "Invitado";
    $scope.errorNombre = "";
    
    $scope.mostrar = false;
    $scope.des = false;
    $scope.noSel = false;
    $scope.noSelGrupo = false;
    $scope.badNum = false;
    $scope.error = false;
    $scope.mostrarGrupo = false;
    $scope.boton = true;
    $scope.mostrarFase = false;
    $scope.editInst = true;
    $scope.editFase = true;
    $scope.editFaseSel = true;
    $scope.mostrarDatosIns = false;
    $scope.errorFase = false;
    
    $scope.instalaciones = {};
    $scope.fases = {};
    $scope.y = {};
    $scope.myFase = {};
    $scope.fasesAdd = {};
    
    
    /*------------------------------------------------------------------------------------------------*/
    /*-------------------------------------- Variables internas --------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Variables de control de las fechas (Simula el paso de los días). BORRAR
    var c = 1;
    $scope.dia = 0;
    var contFases = 5;
    
    var instAux = {};
    var newGrupo = {};
    
    
    /*------------------------------------------------------------------------------------------------*/
    /*-------------------------------------- ARRAY DE FESTIVOS ---------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Al crear las fechas, tener en cuanta que a los meses hay que restarle uno para meterlos.
    var festivos = [
        new Date(2015,3,15),
        new Date(2015,3,16),
        new Date(2015,3,18),
        new Date(2015,3,23),
        new Date(2015,3,24),
        new Date(2015,4,11)
    ];
        
    /*------------------------------------------------------------------------------------------------*/
    /*------------------------------------------- FUNCIONES ------------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Obtenemos las tablas de la BDD, para mostrar la vista principal.
    $http.get('/api/instalaciones')
        .success(function(data){$scope.instalaciones = data;})
        .error(function(data){console.log('Error: '+data);});
    
    $http.get('/api/fases')
        .success(function(data){$scope.fases = data;})
        .error(function(data){console.log('Error: '+data);});
    
    //Cada 24 horas lanzamos una comprobación de fechas, que recorrerá las fases activas y 
    //comprobará su caducidad.
    var timer = $interval(function(){
        //Cerramos la parte detallada de instalación.
        $scope.mostrarDatosIns = false;
        //Si es un día festivo, se le suma un día a la duración de las fases.
        if(esFestivo()){sumarDia();}
        comprobarFechas();
        console.log('Días transcurridos: ', c);
        $scope.dia = c;
        c++;
    }, 20000/*86400000*/);    
    
    //Parsea a dos dígitos
    var twoDigits = function(d) {
        if(0 <= d && d < 10) return "0" + d.toString();
        if(-10 < d && d < 0) return "-0" + (-1*d).toString();
        return d.toString();
    }
    
    /*-------------------------------- Funciones de gestión de fechas --------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Comprobamos que las fases activas estan en tiempo.
    var comprobarFechas = function(){
        //Recorremos las instalaciones.
        for(var i=0; i<$scope.instalaciones.length; i++){
            //Recojo cada una de las IDs de las fases y las busco en el array de fases.
            angular.forEach($scope.instalaciones[i], function(value, key){
                if(key != 'instalacion_id' && key != 'nombre' && value!='descripcion' && value!='prioridad' && value!='fecha' && value!=undefined){        
                    //Recorro el array de fases.
                    for(var k=0; k<$scope.fases.length; k++){
                        //Si la fase se encuentra activa.
                        if($scope.fases[k].fase_id==value && $scope.fases[k].activa && !$scope.fases[k].hecha){
                            //Si se pasa de tiempo la fase.
                            if(fechaMayor($scope.fases[k])){
                                console.log('ERROR: Fase fuera de fecha: ',$scope.instalaciones[i].nombre,' - ', $scope.fases[k].nombre);
                                $scope.instalaciones[i].prioridad=0;
                                $scope.fases[k].error=true;
                                  
                                $scope.instalaciones[i].fecha = new Date($scope.instalaciones[i].fecha);
                                $scope.instalaciones[i].fecha =
                                    $scope.instalaciones[i].fecha.getFullYear()+'-'
                                    +twoDigits($scope.instalaciones[i].fecha.getMonth()+1)+'-'
                                    +$scope.instalaciones[i].fecha.getDate();
                                
                                $http.put('/api/instalacion', $scope.instalaciones[i])
                                    .success(function(data){$scope.instalaciones = data;})
                                    .error(function(data){ console.log('Error: ' + data);});

                                $scope.fases[k].fecha = new Date($scope.fases[k].fecha);
                                $scope.fases[k].fecha = $scope.fases[k].fecha.getFullYear()+'-'+twoDigits($scope.fases[k].fecha.getMonth()+1)+'-'+$scope.fases[k].fecha.getDate();
                                $scope.fases[k].error=true;
                                
                                $http.put('/api/fase', $scope.fases[k])
                                    .success(function(data){$scope.fases = data;})
                                    .error(function(data){console.log('Error: '+data);});
                            }
                        }
                    }                            
                }
            });
        }
        //Después de todas las comprobaciones y posibles cambios, refrescamos la vista de instalaciones.
        $http.get('/api/instalaciones')
            .success(function(data){$scope.instalaciones = data;})
            .error(function(data){console.log('Error: '+data);});
    }
    
    //Comprobamos que la fecha de inicio más la duración no supere la fecha actual
    //Devuelve true si la fase está "caducada".
    var fechaMayor = function(f){        
        var hoy = new Date();
        
        //AQUI ESTÁ TRAMPEADO PARA SIMULAR PASO DEL TIEMPO CON EL CONTADOR 'c'.
        var aux = Date.parse(f.fecha);
        var fechaAux = new Date(aux);
        var dias;    
        dias = ((hoy.getTime()+(c*86400000))-fechaAux.getTime())/86400000;
        //dias = (hoy.getTime()-fechaAux.getTime())/86400000;
        
        if(dias>f.duracion){return true;}        
        return false;
    }
    
    //Si es un día festivo, (está en el array de festivos) devolvemos true.
    var esFestivo = function() {
        var hoy = new Date();
        
        for(var i=0; i<festivos.length; i++){
            if(festivos[i].getDate() == hoy.getDate()){return true;}
        }
        return false;
    }
    
    
    var sumarDia = function(){
        //Recorro el array de fases.
        for(var i=0; i<$scope.fases.length; i++){
            //Comprobamos si está activa o no
            if($scope.fases[i].activa && !$scope.fases[i].hecha){
                $scope.fases[i].duracion++;
                
                //Parseamos las fechas.
                var aux = new Date($scope.fases[i].fecha);
                $scope.fases[i].fecha = aux.getFullYear()+'-'+
                                        twoDigits(aux.getMonth()+1)+'-'+twoDigits(aux.getDate());
                
                $http.put('/api/fase' ,$scope.fases[i])
                    .success(function(data){$scope.fases = data;})
                    .error(function(data){ console.log('Error: ' + data);});               
            }
        }
    }
    
    
    /*-------------------------------- Funciones auxiliares de la app --------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Mostramos el section add, que es el que contiene los formularios.
    //Además, mostramos el formulario de nueva instalación.
    $scope.nuevo = function(){
        $scope.des = true;
        $scope.mostrar = true; 
        
        //Obtenemos las fases de la BDD para mostrarlos en el formulario de nueva instalación.
        $http.get('/api/fases')
            .success(function(data){$scope.fases = data;})
            .error(function(data){console.log('Error: '+data);});
    };
    
    //Comprueba que el parámetro facilitado es un número para mostrar errores de los formularios.
    $scope.esNum = function(num) {        
        if(!angular.isNumber(num)){
            $scope.badNum = true;
        }else{
            $scope.badNum = false;
        }            
    }
    
    //Muestra la parte donde listamos los detalles de una instalación,
    //además de realizar la gestión de la activación y finalización de fases.
    $scope.mostrarDatos = function(obj){
        $scope.mostrarDatosIns = true;
        $scope.idDatos=obj.instalacion_id;
        
        $http.get('/api/instalacion/' + obj.instalacion_id)
            .success(function(data){$scope.fasesAdd = data;})
            .error(function(data){console.log('Error: '+data);});
    }
    
    //Modificamos la variable mostrarDatosIns para ocultar la parte detallada de la instalación.
    $scope.edIns = function(){
        $scope.mostrarDatosIns = false;
    }
    
    
    /*-------------------------------- Funciones de añadir registros ---------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Función para registrar una fase.
    $scope.addFase = function(){
        $scope.f.fecha = undefined;
        $scope.f.hecha = false;
        $scope.f.activa = false;
        
        $http.post('/api/fase', $scope.f)
            .success(function(data){$scope.resetearFormFase(); $scope.fases = data;})
            .error(function(data){console.log('Error: '+data);});
        
        console.log('Fase nueva: ', $scope.f);
    }
                        
    //Función para registrar una instalación
    $scope.addIns = function() {
        var fase = 1;
        var aux = new Date();
        $scope.x.fecha = aux.getFullYear()+'-'+twoDigits(aux.getMonth()+1)+'-'+twoDigits(aux.getDate());
        
        for(var i=0; i<$scope.fases.length;i++){
            if($scope.fases[i].$selected == true){
                if(fase==1){$scope.x.idFase1=$scope.fases[i].fase_id;}
                if(fase==2){$scope.x.idFase2=$scope.fases[i].fase_id;}
                if(fase==3){$scope.x.idFase3=$scope.fases[i].fase_id;}
                if(fase==4){$scope.x.idFase4=$scope.fases[i].fase_id;}
                if(fase==5){$scope.x.idFase5=$scope.fases[i].fase_id;}
                if(fase==6){$scope.x.idFase6=$scope.fases[i].fase_id;}
                if(fase==7){$scope.x.idFase7=$scope.fases[i].fase_id;}
                if(fase==8){$scope.x.idFase8=$scope.fases[i].fase_id;}
                if(fase==9){$scope.x.idFase9=$scope.fases[i].fase_id;}
                if(fase==10){$scope.x.idFase10=$scope.fases[i].fase_id;}
                if(fase==11){$scope.x.idFase11=$scope.fases[i].fase_id;}
                if(fase==12){$scope.x.idFase12=$scope.fases[i].fase_id;}
                if(fase==13){$scope.x.idFase13=$scope.fases[i].fase_id;}
                if(fase==14){$scope.x.idFase14=$scope.fases[i].fase_id;}
                if(fase==15){$scope.x.idFase15=$scope.fases[i].fase_id;}
                if(fase==16){$scope.x.idFase16=$scope.fases[i].fase_id;}
                if(fase==17){$scope.x.idFase17=$scope.fases[i].fase_id;}
                if(fase==18){$scope.x.idFase18=$scope.fases[i].fase_id;}
                if(fase==19){$scope.x.idFase19=$scope.fases[i].fase_id;}
                if(fase==20){$scope.x.idFase20=$scope.fases[i].fase_id;}
                fase++;
            }
        }
        
        if($scope.x.idFase1==undefined){
            $scope.noSel=true;
        }else{
            $http.post('/api/instalacion', $scope.x)
                .success(function(data){ $scope.resetearFormIns(); $scope.instalaciones = data;})
                .error(function(data){console.log('Error: '+ data);});
        }
    };
    
    
    /*------------------------------- Funciones de modificar registros -------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Modificamos la fase y su referencia en el grupo.
    $scope.guardarFaseInst = function(fase, instalacion){        
        var faseGuardar = {}
        var idAux=0;
        var nombre="";
        faseGuardar['nombre']=fase.nombre;
        faseGuardar['duracion']=fase.duracion;
        faseGuardar.base=0;
        faseGuardar.error=fase.error;
        faseGuardar.activa=fase.activa;
        faseGuardar.hecha=fase.hecha;
        
        //Comprobamos que no existe una fase igual.
        for(var i=0; i<$scope.fases.length;i++){
            if($scope.fases[i].nombre==fase.nombre && $scope.fases[i].duracion==fase.duracion && $scope.fases[i].activa==false && $scope.fases[i].fecha==fase.fecha && $scope.fases[i].error==false){
                //Si hay una fase igual, mandamos su id para asociarlo.
                idAux = $scope.fases[i].fase_id;
                break;
            }
        }
                
        //Recorremos la instalación para ver que idFase es la que tenemos que modificar.
        angular.forEach(instalacion, function(value, key){    
            //Recorro los campos idFase y si coincide el valor, cogemos el nombre.
            if(key != 'grupo_id' && key != 'nombre' && value==fase.fase_id){                            
                nombre=key;
            }
        });
        if(fase.base==true){
            if(idAux==0){
                //Primero creamos la nueva fase.
                $http.post('/api/fase', faseGuardar)
                        .success(function(data){$scope.fases = data;})
                        .error(function(data){console.log('Error: '+data);});

                var aux2 = {
                    instalacionId : instalacion.instalacion_id,
                    faseCampo : nombre
                };

                //Cambiamos su referencia en la instalación.
                $http.put('/api/faseUltimaIns', aux2)
                    .success(function(data){$scope.instalaciones = data;})
                    .error(function(data){console.log('Error: '+data);});
            }else{
                //Creamos un objeto auxiliar con el id de la instalación y
                //con las dos id's de fase, antigua y nueva.
                var aux = {
                    instalacionId : instalacion.instalacion_id,
                    faseCampo : nombre,
                    faseId : idAux
                };
                //Cambiamos su referencia en la instalación.
                $http.put('/api/faseIns', aux)
                    .success(function(data){$scope.instalaciones = data;})
                    .error(function(data){console.log('Error: '+data);});
            }
        }else{
            //Parseamos las fechas.
            var aux = new Date(fase.fecha);
            fase.fecha = aux.getFullYear()+'-'+
                                    twoDigits(aux.getMonth()+1)+'-'+twoDigits(aux.getDate());
            
            fase.error=false;
            
            //Si la fase no es base, es propia de la instalación.
            //Por lo tanto, la modificamos y listo
            $http.put('/api/fase' ,fase)
                    .success(function(data){$scope.fases = data;})
                    .error(function(data){ console.log('Error: ' + data);});
        }
        //Recuperamos las fases de la instalación que estabamos editando.
        $scope.mostrarDatos(instalacion);
        console.log('Fase Modificada. Cambiada su id en la instalación');
    }
    
    //Modificamos una instalación.
    $scope.guardarInst = function(instalacion){
        //Parseamos la fecha.
        var aux = new Date(instalacion.fecha);
        instalacion.fecha = aux.getFullYear()+'-'+
                            twoDigits(aux.getMonth()+1)+'-'+twoDigits(aux.getDate());
        
        $http.put('/api/instalacion', instalacion)
            .success(function(data){$scope.instalaciones = data;})
            .error(function(data){console.log('Error: '+data);});
        
        console.log('Instalación modificada: ', instalacion.instalacion_id + '-' + instalacion.nombre);  
    }
    
    
    /*------------------------------- Funciones de eliminar registros --------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Eliminamos la instalación facilitada.
    $scope.eliminarInst = function(x){
        $http.delete('/api/instalacion/' + x.instalacion_id)
            .success(function(data){$scope.instalaciones = data;})
            .error(function(data){console.log('Error: ' + data);});
    }
    
    
    /*------------------------------------- Funciones manejo fases -----------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Iniciamos la fase facilitada.
    $scope.iniciarFase = function(fase, instalacion){
        var nombre="";
        //Recorremos la instalación para ver que idFase es la que tenemos que modificar.
        angular.forEach(instalacion, function(value, key){    
            //Recorro los campos idFase y si coincide el valor, cogemos el nombre.
            if(key != 'grupo_id' && key != 'nombre' && value==fase.fase_id){
                nombre=key;
            }
        });
        
        fase.activa = true;
        var aux = new Date();
        fase.fecha = aux.getFullYear()+'-'+twoDigits(aux.getMonth()+1)+'-'+twoDigits(aux.getDate());
        fase.error = false;
        
        //Como vamos a modificar valores de la fase,si es base, creamos una nueva.
        if(fase.base==true){
            fase.fase_id=undefined;
            //Primero creamos la nueva fase.
            $http.post('/api/fase', fase)
                .success(function(data){$scope.fases = data;})
                .error(function(data){console.log('Error: '+data);});

            var aux = {
                instalacionId : instalacion.instalacion_id,
                faseCampo : nombre
            };

            //Cambiamos su referencia en la instalación.
            $http.put('/api/faseUltimaIns', aux)
                .success(function(data){$scope.instalaciones = data;})
                .error(function(data){console.log('Error: '+data);});
        }else{
            $http.put('/api/fase', fase)
                .success(function(data){$scope.fases = data;})
                .error(function(data){console.log('Error: '+data);});
        }
        
        //Actualizamos la lista de fases de la instalación.
        $http.get('/api/instalacion/' + instalacion.instalacion_id)
            .success(function(data){$scope.fasesAdd = data;})
            .error(function(data){console.log('Error: '+data);});
        
        console.log('Fase iniciada: ', instalacion.nombre,' -> ',fase.nombre);
    }
        
    //Finalizamos la fase facilitada.
    $scope.finalizarFase = function(instalacion, fase){
        //Modificamos la variable hecha para indicar que se terminó la fase
        //y actualizamos la tabla de fases.
        fase.hecha=true;
        var aux = new Date();
        fase.fecha = aux.getFullYear()+'-'+twoDigits(aux.getMonth()+1)+'-'+twoDigits(aux.getDate());
        fase.error=false;
        $http.put('/api/fase', fase)
            .success(function(data){$scope.fases = data;})
            .error(function(data){console.log('Error: '+data);});
        
        //Una vez actualizada la tabla de fases, actualizamos la tabla de fases-instalación
        $http.get('/api(instalacion/'+instalacion.instalacion_id)
            .success(function(data){$scope.fasesAdd = data;})
            .error(function(data){console.log('Error: '+data);});
        
        console.log('Fase finalizada: ', instalacion.nombre, ' -> ', fase.nombre);
    }    
    
    
    /*------------------------------------- Reset de Formularios -------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Reseteamos el formulario de Instalaciones.
    $scope.resetearFormIns = function(){
        $scope.x=undefined;
        $scope.mostrar = false;
        $scope.mostrarGrupo = false;
        $scope.mostrarFase = false;
        $scope.des = false;
        $scope.badNum = false;  
        $scope.error = false;
        $scope.noSel=false;
        $scope.grupoSel = "";
        $scope.fasesGrupoSel = "";
        $scope.myGrupo = {};
        $scope.formIns.$setPristine();
    }
    
    //Reseteamos el formulario de Fases.
    $scope.resetearFormFase = function() {
        $scope.formFase.$setPristine();
        $scope.mostrarFase = false;
        $scope.f = undefined;
    }
});