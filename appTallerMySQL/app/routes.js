//var Fase = require('./modelo/fase');
//var Grupo = require('./modelo/grupo');
//var Ins = require('./modelo/instalacion');
var Controller = require('./controller');

module.exports = function(app){
    
/*-------------------------- Funciones específicas de fases -------------------------*/
/*-----------------------------------------------------------------------------------*/
    //Devolvemos todas las fases.
    app.get('/api/fases', Controller.getFases);
    
    //Creamos una fase.
    app.post('/api/fase', Controller.setFase);
    
    //Modificamos la duración de una fase.
    app.put('/api/faseIns', Controller.updateFaseIns);
    
    //Añadimos la ultima fase a la instalación.
    app.put('/api/faseUltimaIns', Controller.updateFaseUltimaIns);
    
    //Modificamos valores de una fase
    app.put('/api/fase', Controller.updateFase);
    
    
/*---------------------- Funciones específicas de instalaciones ---------------------*/
/*-----------------------------------------------------------------------------------*/
    //Devolvemos todas las instalaciones
    app.get('/api/instalaciones', Controller.getIns);
    
    //Buscamos las fases de una instalación.
    app.get('/api/instalacion/:instalacion_id', Controller.findFases);
    
    //Creamos una instalación.
    app.post('/api/instalacion', Controller.setIns);
    
    //Modificamos una instalación.
    app.put('/api/instalacion', Controller.updateIns);
    
    //Eliminamos una instalación
    app.delete('/api/instalacion/:instalacion_id', Controller.removeIns);
    
    
/*------------------------------- Llamadas auxiliares -------------------------------*/
/*-----------------------------------------------------------------------------------*/    
    
    //Para el resto de llamadas, devolvemos el index.html (vista única de la app).
    app.get('*', function(req, res){
        res.sendfile('./angular/index.html');
    });
};