var mongoose = require ('mongoose');

module.exports = mongoose.model(
    'Fase', {
        nombre: String,
        duracion: String,
        fecha: Date,
        activa: Boolean,
        hecha: Boolean
    }
);