var mongoose = require ('mongoose');

module.exports = mongoose.model(
    'Instalacion', {
        nombre: String,
        desc: String,
        prioridad: Number,
        fecha: {type: Date, default: Date.now}
        idGrupo: String
    }
);