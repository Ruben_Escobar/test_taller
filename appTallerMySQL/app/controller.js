var mysql = require('mysql');
var connectionpool = mysql.createPool({
    host    :   'localhost',
    user    :   'root',
    password:   '',
    database:   'bddMySQL'
});


/*-------------------------- Funciones específicas de fases -------------------------*/
/*-----------------------------------------------------------------------------------*/

//Obtenemos todos los objetos fase de la tabla FASES
exports.getFases = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           console.error('CONNECTION error: ',err);
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
        connection.query('SELECT * FROM fases', function(err, rows, fields){
            if(err){
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
            res.json(rows);
            }
            connection.release();
        });
       }
    });
}

//Guarda un objeto fase en la BDD
exports.setFase = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
           connection.query("INSERT INTO fases (nombre, duracion, fecha, hecha, activa, error)"
                    +" values (?,?,STR_TO_DATE(?, '%Y-%m-%d'),?,?,?)",[req.body.nombre, req.body.duracion, req.body.fecha, req.body.hecha, req.body.activa, req.body.error], function(err, rows, fields){
               
               
               console.log('Elementos de la fase: ',req.body.nombre, req.body.duracion, req.body.fecha, req.body.hecha, req.body.activa, req.body.error);
               
               
               if(err){
                   console.error(err);
                    res.statusCode = 500;
                    res.send({
                        result: 'error',
                        err:    err.code
                    });
                }else{
                    console.log(exports.getFases(req,res));
                    exports.getFases(req,res);
                }
                //res.json(rows);
                connection.release();
            });
        }
        console.error('Consulta setFase correcta.');
    });
}

//Modificamos una fase.
exports.updateFase = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
           var srt = "UPDATE fases SET nombre=?, duracion=?,"+
               " fecha=STR_TO_DATE(?, '%Y-%m-%d'),"+
               " hecha=?, activa=?, base=?, error=? WHERE fase_id=?";
        connection.query(srt, [req.body.nombre, req.body.duracion, req.body.fecha, req.body.hecha, req.body.activa, req.body.base, req.body.error, req.body.fase_id], function(err, rows, fields){
            if(err){
                console.log('error');
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
            exports.getFases(req, res);
            //res.json(rows);
            }
            connection.release();
        });
       }
    });
    console.error('Consulta updateFase correcta.');
}

//Modificamos la duración de una fase y actualizamos su enlace en Instalaciones.
exports.updateFaseIns = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
           var srt = 'UPDATE instalaciones SET '+req.body.faseCampo+' = '+
               req.body.faseId+' WHERE instalacion_id=?';
        connection.query(srt, req.body.instalacionId, function(err, rows, fields){
            if(err){
                console.log('error');
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
            exports.getIns(req, res);
            //res.json(rows);
            }
            connection.release();
        });
       }
    });
    console.error('Consulta updateFaseIns correcta.');
}

//Enlazamos la nueva fase creada con su instalación correspondiente.
exports.updateFaseUltimaIns = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
           var srt = 'UPDATE instalaciones SET '+req.body.faseCampo+' = '+
               '(SELECT fase_id FROM fases ORDER BY fase_id DESC LIMIT 1)'+
               ' WHERE instalacion_id=?';
        connection.query(srt, req.body.instalacionId, function(err, rows, fields){
            if(err){
                console.log('error');
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
            exports.getIns(req, res);
            //res.json(rows);
            }
            connection.release();
        });
       }
    });
    console.error('Consulta updateFaseUltimaIns correcta.');
}


/*--------------------- Funciones específicas de instalaciones ----------------------*/
/*-----------------------------------------------------------------------------------*/

//Obtenemos todos los objetos instalacion de la tabla instalaciones.
exports.getIns = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           console.error('CONNECTION error: ',err);
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
        connection.query('SELECT * FROM instalaciones', function(err, rows, fields){
            if(err){
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
             res.json(rows);
            }
            connection.release();
        });
       }
    });
}

//Guarda un objeto instalación en la BDD
exports.setIns = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
           var srt ="INSERT INTO instalaciones (nombre, descripcion, prioridad,"+
                    " fecha, idFase1, idFase2, idFase3, idFase4, idFase5, idFase6,"+
                    " idFase7, idFase8, idFase9, idFase10, idFase11, idFase12,"+
                    " idFase13, idFase14, idFase15, idFase16, idFase17, idFase18,"+
                    " idFase19, idFase20)"+
                    " values (?, ?, ?, STR_TO_DATE(?, '%Y-%m-%d'), ?, ?, ?, ?, ?, ?,"+
                    " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
           connection.query(srt,
                [req.body.nombre, req.body.descripcion, req.body.prioridad,
                 req.body.fecha, req.body.idFase1, req.body.idFase2, req.body.idFase3,
                 req.body.idFase4, req.body.idFase5, req.body.idFase6,
                 req.body.idFase7, req.body.idFase8, req.body.idFase9,
                 req.body.idFase10, req.body.idFase11, req.body.idFase12,
                 req.body.idFase13, req.body.idFase14, req.body.idFase15,
                 req.body.idFase16, req.body.idFase17, req.body.idFase18,
                 req.body.idFase19, req.body.idFase20], function(err, rows, fields){
               
               
               console.log('Elementos de la fase: ',req.body.nombre, req.body.duracion, req.body.fecha, req.body.hecha, req.body.activa, req.body.error);
               
               
               if(err){
                   console.error(err);
                    res.statusCode = 500;
                    res.send({
                        result: 'error',
                        err:    err.code
                    });
                }else{
                    exports.getIns(req,res);
                }
                //res.json(rows);
                connection.release();
            });
        }
        console.error('Consulta setIns correcta.');
    });
    /*req.body.idFase1, req.body.idFase2, req.body.idFase3, req.body.idFase4, req.body.idFase5, req.body.idFase6, req.body.idFase7, req.body.idFase8, req.body.idFase9, req.body.idFase10, req.body.idFase11, req.body.idFase12, req.body.idFase13, req.body.idFase14, req.body.idFase15, req.body.idFase16, req.body.idFase17, req.body.idFase18, req.body.idFase19, req.body.idFase20*/
}

//Modificamos la instalación.
exports.updateIns = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
           var srt = "UPDATE instalaciones SET nombre=?, descripcion=?, prioridad=?,"+
               "fecha=STR_TO_DATE(?, '%Y-%m-%d') WHERE instalacion_id=?";
        connection.query(srt, [req.body.nombre, req.body.descripcion, req.body.prioridad, req.body.fecha, req.body.instalacion_id], function(err, rows, fields){
            if(err){
                console.log('error');
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
            exports.getIns(req, res);
            //res.json(rows);
            }
            connection.release();
        });
       }
    });
    console.error('Consulta updateIns correcta.');
}

//Borrar una instalacion
exports.removeIns = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           console.error('CONNECTION error: ',err);
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
        connection.query('DELETE FROM instalaciones WHERE instalacion_id=?',req.params.instalacion_id, function(err, rows, fields){
            if(err){
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }else{
             exports.getIns(req, res);
            }
            connection.release();
        });
       }
    });
}


/*----------------------------- Funciones Auxiliares --------------------------------*/
/*-----------------------------------------------------------------------------------*/

exports.findFases = function(req, res){
    //Iniciamos la conexión de MySQL.
    connectionpool.getConnection(function(err, connection){
       if(err){
           res.statusCode = 503;
           res.send({result: 'error', err: err.code});
       }else{
        connection.query('SELECT f.fase_id, f.nombre, f.duracion, f.fecha, f.hecha, f.activa, f.base, f.error FROM fases f LEFT JOIN instalaciones ON fase_id=idFase1 OR fase_id=idFase2 OR fase_id=idFase3 OR fase_id=idFase4 OR fase_id=idFase5 OR fase_id=idFase6 OR fase_id=idFase7 OR fase_id=idFase8 OR fase_id=idFase9 OR fase_id=idFase10 OR fase_id=idFase11 OR fase_id=idFase12 OR fase_id=idFase13 OR fase_id=idFase14 OR fase_id=idFase15 OR fase_id=idFase16 OR fase_id=idFase17 OR fase_id=idFase18 OR fase_id=idFase19 OR fase_id=idFase20 WHERE instalacion_id = ?', [req.params.instalacion_id], function(err, rows, fields){
            if(err){
                console.log('error');
                console.error(err);
                res.statusCode = 500;
                res.send({
                    result: 'error',
                    err:    err.code
                });
            }
            res.json(rows);
            connection.release();
        });
       }
    });
    console.error('Consulta findFases correcta.');
}