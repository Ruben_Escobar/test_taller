//Inicialización de variables
// importamos express y mongoose e indicamos el puerto por el que será accesible la app.
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var port = process.env.port || 8081;

//Configuración del servidor

//Conexión a la BDD mongo
mongoose.connect('mongodb://localhost:27017/bddTaller');

app.configure(function(){
    app.use(express.static(__dirname + '/angular'));    //Creamos el midleware estático.
    app.use(express.logger('dev'));                     //Activamos el log de los request.
    app.use(express.bodyParser());                      //Extrae la información del HTML en las llamadas POST.
    app.use(express.methodOverride());                  //Simula DELETE y PUT.
});

//Cargamos los endpoints
require('./app/routes.js')(app);

//Ponemos a escuchar al servidor en el puerto designado.
app.listen(port);
console.log('App por el puerto ' + port);