var Fase = require('./modelo/fase');
var Grupo = require('./modelo/grupo');
var Ins = require('./modelo/instalacion');
var Controller = require('./controller');

module.exports = function(app){

    /*-------------------- Funciones específicas de fases --------------------*/
    //Devolvemos todas las fases.
    app.get('/api/fases', Controller.getFases);
    //Creamos una fase.
    app.post('/api/fase', Controller.setFase);
    //Modificamos la duración de una fase.
    app.put('/api/fase/:fase_id', Controller.updateFase);
    //Borrar una fase.
    app.delete('/api/fase/:fase_id', Controller.removeFase);
    
    
    /*-------------------- Funciones específicas de grupos --------------------*/
    //Devolvemos todos los grupos.
    app.get('/api/grupos', Controller.getGrupos);
    //Creamos un grupo.
    app.post('/api/grupo', Controller.setGrupo);
    //Modificamos el grupo.
    app.put('/api/grupo/:grupo_id', Controller.updateGrupo);
    //Borrar un grupo.
    app.delete('/api/grupo/:grupo_id', Controller.removeGrupo);
    
    
    /*----------------- Funciones específicas de instalaciones -----------------*/
    //Devolvemos todas las instalaciones
    app.get('/api/instalaciones', Controller.getIns);
    //Creamos una instalación.
    app.post('/api/instalacion', Controller.setIns);
    //Modificamos una instalación.
    app.put('/api/instalacion/:instalacion_id', Controller.updateIns);
    //Eliminamos una instalación
    app.delete('/api/instalacion/:instalacion_id', Controller.removeIns);
    
    
    //Para el resto de llamadas, devolvemos el index.html (vista única de la app).
    app.get('*', function(req, res){
        res.sendfile('./angular/index.html');
    });
    
    //Llamadas auxiliares.
    app.get('/api/grupo/:id', Controller.findById);
};