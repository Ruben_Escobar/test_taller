var mongoose = require ('mongoose');

module.exports = mongoose.model(
    'Grupo', {
        nombre: String,
        idFase1: String,
        idFase2: String,
        idFase3: String,
        idFase4: String,
        idFase5: String,
        idFase6: String,
        idFase7: String,
        idFase8: String,
        idFase9: String,
        idFase10: String,
        idFase11: String,
        idFase12: String,
        idFase13: String,
        idFase14: String,
        idFase15: String,
        idFase16: String,
        idFase17: String,
        idFase18: String,
        idFase19: String,
        idFase20: String
    }
);