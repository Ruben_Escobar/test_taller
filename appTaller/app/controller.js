var Fase = require('./modelo/fase');
var Grupo = require('./modelo/grupo');
var Ins = require('./modelo/instalacion');

/*-------------------- Funciones específicas de fases --------------------*/
//Obtenemos todos los objetos fase de la tabla FASES
exports.getFases = function(req, res){
    Fase.find(
        function(err, fase){
            if(err){res.send(err);}
            res.json(fase);         //Devolvemos los objetos JSON
        }
    );
}

//Guarda un objeto fase en la BDD
exports.setFase = function(req, res){
    Fase.create(
        {nombre: req.body.nombre, duracion: req.body.duracion, fecha: req.body.fecha, hecha: req.body.hecha, activa: req.body.activa},
        function(err, fase){
            if(err){res.send(err);}
            //Una vez que insertamos la fase, devolvemos la lista de fases.
            Fase.find( function(err, fase){ if(err){res.send(err);} res.json(fase);});
        }
    );
}

//Modificamos la duración de una fase.
exports.updateFase = function(req, res){
    Fase.update(
        {_id: req.params.fase_id},
        {$set:{nombre: req.body.nombre, duracion: req.body.duracion, fecha: req.body.fecha, hecha: req.body.hecha, activa: req.body.activa}},
        function(err, fase){
            if(err){res.send(err);}
            //Una vez que modificamos la fase, devolvemos la lista de fases.
            Fase.find( function(err, fase){ if(err){res.send(err);} res.json(fase);});
        }
    );
}


//Borrar una fase.
exports.removeFase = function(req, res){
    Fase.remove(
        {_id: req.params.fase_id},
        function(err, fase){
            if(err){res.send(err);}
            //Una vez que eliminada la fase, devolvemos la lista de fases.
            Fase.find( function(err, fase){ if(err){res.send(err);} res.json(fase);});
        }
    );
}

/*-------------------- Funciones específicas de grupos --------------------*/
//Obtenemos todos los objetos grupo de la tabla grupos.
exports.getGrupos = function(req, res){
    Grupo.find(
        function(err, grupo){
            if(err){res.send(err);}
            res.json(grupo);
        }
    );
}

//Guarda un objeto grupo en la BDD
exports.setGrupo = function(req, res){
    Grupo.create(
        {nombre: req.body.nombre, idFase1: req.body.idFase1, idFase2: req.body.idFase2, idFase3: req.body.idFase3, idFase4: req.body.idFase4, idFase5: req.body.idFase5, idFase6: req.body.idFase6, idFase7: req.body.idFase7, idFase8: req.body.idFase8, idFase9: req.body.idFase9, idFase10: req.body.idFase10, idFase11: req.body.idFase11, idFase12: req.body.idFase12, idFase13: req.body.idFase13, idFase14: req.body.idFase14, idFase15: req.body.idFase15, idFase16: req.body.idFase16, idFase17: req.body.idFase17, idFase18: req.body.idFase18, idFase19: req.body.idFase19, idFase20: req.body.idFase20},
        function(err, grupo){
            if(err){res.send(err);}
            //Una vez insertado el grupo, devolvemos la lista de grupos.
            Grupo.find(function(err, grupo){ if(err){res.send(err);} res.json(grupo); });
        }
    );
}

//Modificamos el grupo.
exports.updateGrupo = function(req, res){
    Grupo.update(
        {_id: req.params.grupo_id},
        {$set: {nombre: req.body.nombre, idFase1: req.body.idFase1, idFase2: req.body.idFase2, idFase3: req.body.idFase3, idFase4: req.body.idFase4, idFase5: req.body.idFase5, idFase6: req.body.idFase6, idFase7: req.body.idFase7, idFase8: req.body.idFase8, idFase9: req.body.idFase9, idFase10: req.body.idFase10, idFase11: req.body.idFase11, idFase12: req.body.idFase12, idFase13: req.body.idFase13, idFase14: req.body.idFase14, idFase15: req.body.idFase15, idFase16: req.body.idFase16, idFase17: req.body.idFase17, idFase18: req.body.idFase18, idFase19: req.body.idFase19, idFase20: req.body.idFase20}},
        function(err, grupo){
            if(err){res.send(err);}
            //Una vez insertado el grupo, devolvemos la lista de grupos.
            Grupo.find(function(err, grupo){ if(err){res.send(err);} res.json(grupo); });
        }        
    );
}
//Borrar un grupo.
exports.removeGrupo = function(req, res){
    Grupo.remove(
        {_id: req.params.grupo_id},
        function(err, grupo){
            if(err){res.send(err);}
            //Una vez eliminado el grupo, devolvemos la lista de grupos.
            Grupo.find(function(err, grupo){ if(err){res.send(err);} res.json(grupo); });
        }
    );
}

/*----------------- Funciones específicas de instalaciones -----------------*/
//Obtenemos todos los objetos instalacion de la tabla instalaciones.
exports.getIns = function(req, res){
    Ins.find(
        function(err, inst){
            if(err){res.send(err);}
            res.json(inst);
        }
    );
}
//Guarda un objeto instalación en la BDD
exports.setIns = function(req, res){
    Ins.create(
        {nombre: req.body.nombre, desc: req.body.desc, prioridad: req.body.prioridad, fecha: req.body.fecha, idGrupo: req.body.idGrupo},
        function(err, inst){
            if(err){res.send(err);}
            //Una vez insertada la instalación, devolvemos la lista de instalaciones.
            Ins.find(function(err, inst){ if(err){res.send(err);} res.json(inst);});
        }
    );
}

//Modificamos la instalación.
exports.updateIns = function(req, res){
    Ins.update(
        {_id: req.params.instalacion_id},
        {$set:{nombre: req.body.nombre, desc: req.body.desc, prioridad: req.body.prioridad, fecha: req.body.fecha, grupo: req.body.grupo}},
        function(err, inst){
            if(err){res.send(err);}
            //Una vez modificada la instalación, devolvemos la lista de instalaciones.
            Ins.find(function(err, inst){ if(err){res.send(err);} res.json(inst);});
        }
    );
}

//Borrar una instalacion
exports.removeIns = function(req, res){
    Ins.remove(
        {_id: req.params.instalacion_id},
        function(err, inst){
            if(err){res.send(err);}
            //Una vez modificada la instalación, devolvemos la lista de instalaciones.
            Ins.find(function(err, inst){ if(err){res.send(err);} res.json(inst);});
        }
    );
}

//buscar un grupo por id
exports.findById = function(req, res){
    Grupo.findOne({_id: req.params.grupo_id}, function(err, grupo){
        if(err){res.send(err);}
        res.json(grupo);
    });
}