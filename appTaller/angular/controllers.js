"use strict";   //Uso estricto de javaScript
app.controller('insCtr', function ($scope, ngTableParams, $timeout, $interval, $http){
    
    /*------------------------------------------------------------------------------------------------*/
    /*------------------------------------- Variables del modelo -------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    $scope.cont = 1;
    
    $scope.grupoSel = "";
    $scope.fasesGrupoSel = "";
    $scope.tuNombre = "Invitado";
    $scope.errorNombre = "";
    
    $scope.mostrar = false;
    $scope.des = false;
    $scope.noSel = false;
    $scope.noSelGrupo = false;
    $scope.badNum = false;
    $scope.error = false;
    $scope.mostrarGrupo = false;
    $scope.boton = true;
    $scope.mostrarFase = false;
    $scope.editInst = true;
    $scope.editFase = true;
    $scope.editFaseSel = true;
    $scope.mostrarDatosIns = false;
    $scope.errorFase = false;
    
    $scope.instalaciones = {};
    $scope.grupos = {};
    $scope.fases = {};
    $scope.y = {};
    $scope.myFase = {};
    $scope.myGrupo = {};
    $scope.fasesAdd = [];
    
    
    /*------------------------------------------------------------------------------------------------*/
    /*-------------------------------------- Variables internas --------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Variables de control de las fechas (Simula el paso de los días). BORRAR
    var c = 1;
    $scope.dia = 0;
    
    var instAux = {};
    var grupoAux = {};
    var newGrupo = {};
    
    
    /*------------------------------------------------------------------------------------------------*/
    /*-------------------------------------- ARRAY DE FESTIVOS ---------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Al crear las fechas, tener en cuanta que a los meses hay que restarle uno para meterlos.
    var festivos = [
        new Date(2015,3,15),
        new Date(2015,3,16),
        new Date(2015,3,18),
        new Date(2015,3,23),
        new Date(2015,3,24),
        new Date(2015,3,27)
    ];
        
    /*------------------------------------------------------------------------------------------------*/
    /*------------------------------------------- FUNCIONES ------------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Obtenemos las tablas de la BDD, para mostrar la vista principal.
    $http.get('/api/instalaciones')
        .success(function(data){$scope.instalaciones = data;})
        .error(function(data){console.log('Error: '+data);});
    
    $http.get('/api/grupos')
        .success(function(data){$scope.grupos = data;})
        .error(function(data){console.log('Error: '+data);});
    
    $http.get('/api/fases')
        .success(function(data){$scope.fases = data;})
        .error(function(data){console.log('Error: '+data);});
    
    //Cada 24 horas lanzamos una comprobación de fechas, que recorrerá las fases activas y 
    //comprobará su caducidad.
    var timer = $interval(function(){
        //Si es un día festivo, se le suma un día a la duración de las fases.
        if(esFestivo()){sumarDia();}
        comprobarFechas();
        console.log('Días transcurridos: ', c);
        $scope.dia = c;
        c++;
    }, 20000/*86400000*/);    
    /*Para cancelar el $interval:
        var killInterval = function(){
            if(angular.isDefined(timer)){
                $interval.cancel(timer);
                timer=undefined;
            }
    }*/
    
    
    /*-------------------------------- Funciones de gestión de fechas --------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Comprobamos que las fases activas estan en tiempo.
    var comprobarFechas = function(){
        //Recorremos las instalaciones.
        for(var i=0; i<$scope.instalaciones.length; i++){
            //Recorremos los grupos.
            for(var j=0; j<$scope.grupos.length; j++){
                if($scope.instalaciones[i].idGrupo==$scope.grupos[j]._id){
                    var faseAux = {};
                    //Recojo cada una de las IDs de las fases y las busco en el array de fases.
                    angular.forEach($scope.grupos[j], function(value, key){
                        if(key != '_id' && key != 'nombre' && value!=undefined){                            
                            //Recorro el array de fases.
                            for(var k=0; k<$scope.fases.length; k++){
                                if($scope.fases[k]._id==value){                                    
                                    angular.copy($scope.fases[k], faseAux);
                                    if(faseAux.activa && !faseAux.hecha){
                                        if(fechaMayor(faseAux)){
                                            console.log('ERROR: Fase fuera de fecha: ',$scope.instalaciones[i].nombre,' - ', faseAux.nombre);
                                            $scope.instalaciones[i].prioridad=0;

                                            $http.put('/api/instalacion/' + $scope.instalaciones[i]._id)
                                                .success(function(data){$scope.instalaciones = data;})
                                                .error(function(data){ console.log('Error: ' + data);});
                                        }
                                    }
                                }
                            }                            
                        }
                    });
                }
            }                 
        }        
    }
    
    //Comprobamos que la fecha de inicio más la duración no supere la fecha actual
    //Devuelve true si la fase está "caducada".
    var fechaMayor = function(f){        
        var aux = new Date();
        var hoy = new Date(aux.getFullYear()+'-'+aux.getMonth()+'-'+aux.getDate());
        //AQUI ESTÁ TRAMPEADO PARA SIMULAR PASO DEL TIEMPO CON EL CONTADOR 'c'.
        var fechaAux = new Date(f.fecha.getFullYear()+'-'+f.fecha.getMonth()+'-'+(f.fecha.getDate()-c));
        var dias;    
        dias = (hoy.getTime()-fechaAux.getTime())/86400000;
        
        if(dias>f.duracion){return true;}        
        return false;
    }
    
    //Si es un día festivo, (está en el array de festivos) devolvemos true.
    var esFestivo = function() {
        var hoy = new Date();
        
        for(var i=0; i<festivos.length; i++){
            if(festivos[i].getDate() == hoy.getDate()){return true;}
        }
        return false;
    }
    
    
    var sumarDia = function(){
        for(var i=0; i<$scope.instalaciones.length; i++){
            //Recorremos los grupos.
            for(var j=0; j<$scope.grupos.length; j++){
                if($scope.instalaciones[i].idGrupo==$scope.grupos[j]._id){
                    var faseAux = {};
                    //Recojo cada una de las IDs de las fases y las busco en el array de fases.
                    angular.forEach($scope.grupos[j], function(value, key){
                        if(key != '_id' && key != 'nombre' && value!=undefined){                            
                            //Recorro el array de fases.
                            for(var k=0; k<$scope.fases.length; k++){
                                //Si coinciden los IDs, es una fase seleccionada
                                if($scope.fases[k]._id==value){                                    
                                    angular.copy($scope.fases[k], faseAux);
                                    if(faseAux.activa && !faseAux.hecha){
                                        faseAux.duracion++;
                                        $http.put('/api/fase/' + faseAux._id)
                                            .success(function(data){$scope.fases = data;})
                                            .error(function(data){ console.log('Error: ' + data);});
                                    }
                                }
                            }                            
                        }
                    });
                }
            }                 
        }
        
        //Recorro las instalaciones.
        for(var i=0; i< instalaciones.length; i++){
            //Recorro las fases.
            for(var j=0; j< instalaciones[i].fases.length; j++){
                //Si la fase está activa, se le añade un día a su duración.
                if(instalaciones[i].fases[j].activa && !instalaciones[i].fases[j].hecha){
                    instalaciones[i].fases[j].duracion++;
                }
            }        
        }
    }
    
    
    
    /*-------------------------------- Funciones auxiliares de la app --------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Mostramos el section add, que es el que contiene los formularios.
    //Además, mostramos el formulario de nueva instalación.
    $scope.nuevo = function(){
        $scope.des = true;
        $scope.mostrar = true; 
        
        //Obtenemos los grupos de la BDD para mostrarlos en el select del form de instalación.
        $http.get('/api/grupos')
            .success(function(data){$scope.grupos = data;})
            .error(function(data){console.log('Error: '+data);});
    };
    
    //Mostramos el formulario de añadir grupos.
    $scope.nuevoGrupo = function(){
        $scope.mostrarGrupo = true;
        
        //Obtenemos las fases de la BDD para mostrarlas en la tabla del form del grupo.
        $http.get('/api/fases')
            .success(function(data){$scope.fases = data;})
            .error(function(data){console.log('Error: '+data);});
    };
    
    //Comprueba que el parámetro facilitado es un número para mostrar errores de los formularios.
    $scope.esNum = function(num) {        
        if(!angular.isNumber(num)){
            $scope.badNum = true;
        }else{
            $scope.badNum = false;
        }            
    }
    
    //Muestra la parte donde listamos los detalles de una instalación,
    //además de realizar la gestión de la activación y finalización de fases.
    $scope.mostrarDatos = function(obj){
        console.log(obj);
        console.log('Fecha: ',obj.fecha.getDate);
        $scope.mostrarDatosIns = true;
        $scope.idDatos=obj._id;
    }    
    
    var faseId = function(id){
        for(var i=0; $scope.fases.length;i++){
            if($scope.fases[i]._id==id){
                return $scope.fases[i];
            }
        }
        return {};
    }
    
    /*-------------------------------- Funciones de añadir registros ---------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    //Función para registrar una fase.
    $scope.addFase = function(){
        $scope.f.fecha = undefined;
        $scope.f.hecha = false;
        $scope.f.activa = false;
        
        $http.post('/api/fase', $scope.f)
            .success(function(data){$scope.resetearFormFase(); $scope.fases = data;})
            .error(function(data){console.log('Error: '+data);});
        
        console.log('Fase nueva: ', $scope.f);
    }
    
    //Función para registrar un grupo.
    $scope.addGrupo = function(){
        $scope.noSel = false;
        var fase = 1;
        
        for(var i=0; i<$scope.fases.length;i++){
            if($scope.fases[i].$selected == true){
                
                /*Prueba con forEach, creo que no es posible.
                angular.forEach($scope.grupo, function(value, key){
                    if(key != '_id' && key != 'nombre' && value==undefined){                            
                        $scope.grupo[key] = $scope.fases[i]._id;
                    }
                });*/
                
                /*NO ENCUENTRO UNA MANERA MEJOR DE HACERLO*/
                if(fase==1){$scope.grupo.idFase1=$scope.fases[i]._id;}
                if(fase==2){$scope.grupo.idFase2=$scope.fases[i]._id;}
                if(fase==3){$scope.grupo.idFase3=$scope.fases[i]._id;}
                if(fase==4){$scope.grupo.idFase4=$scope.fases[i]._id;}
                if(fase==5){$scope.grupo.idFase5=$scope.fases[i]._id;}
                if(fase==6){$scope.grupo.idFase6=$scope.fases[i]._id;}
                if(fase==7){$scope.grupo.idFase7=$scope.fases[i]._id;}
                if(fase==8){$scope.grupo.idFase8=$scope.fases[i]._id;}
                if(fase==9){$scope.grupo.idFase9=$scope.fases[i]._id;}
                if(fase==10){$scope.grupo.idFase10=$scope.fases[i]._id;}
                if(fase==11){$scope.grupo.idFase11=$scope.fases[i]._id;}
                if(fase==12){$scope.grupo.idFase12=$scope.fases[i]._id;}
                if(fase==13){$scope.grupo.idFase13=$scope.fases[i]._id;}
                if(fase==14){$scope.grupo.idFase14=$scope.fases[i]._id;}
                if(fase==15){$scope.grupo.idFase15=$scope.fases[i]._id;}
                if(fase==16){$scope.grupo.idFase16=$scope.fases[i]._id;}
                if(fase==17){$scope.grupo.idFase17=$scope.fases[i]._id;}
                if(fase==18){$scope.grupo.idFase18=$scope.fases[i]._id;}
                if(fase==19){$scope.grupo.idFase19=$scope.fases[i]._id;}
                if(fase==20){$scope.grupo.idFase20=$scope.fases[i]._id;}
                fase++;
            }
        }
                
        if($scope.grupo.idFase1==undefined){
            $scope.noSel = true;
        }else{
            $http.post('/api/grupo', $scope.grupo)
                .success(function(data){$scope.resetearFormGrupo(); $scope.grupos = data;})
                .error(function(data){console.log('Error: '+data);});
            
            console.log('Grupo nuevo: ', $scope.grupo);
        }
    };
        
    //Función para registrar una instalación
    $scope.addIns = function() {
        $scope.x.idGrupo = $scope.myGrupo._id;
        $scope.x.fecha = new Date();  
        
        $http.post('/api/instalacion', $scope.x)
            .success(function(data){console.log('Instalación nueva: ', $scope.x); $scope.resetearFormIns(); $scope.instalaciones = data; console.log('Instalaciones: ', $scope.instalaciones);})
            .error(function(data){console.log('Error: '+ data);});
    };
    
    
    
    /*------------------------------- Funciones de modificar registros -------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    
    
    /*------------------------------- Funciones de eliminar registros --------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    $scope.eliminarInst = function(x){
        $http.delete('api/instalacion/' + x._id)
            .success(function(data){$scope.instalaciones = data;})
            .error(function(data){console.log('Error: ' + data);});
    }
    
    
    
    /*------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
                
    $scope.iniciarFase = function(fase, instalacion){
        //Modificamos las variables activa y fecha de la fase de la instalación.
        for(var i=0; i<instalaciones.length; i++){   
            if(instalaciones[i].id==instalacion.id){
                for(var j=0; j<instalaciones[i].fases.length;j++){
                    if(instalaciones[i].fases[j].id==fase.id){
                        $scope.instalaciones[i].fases[j].activa = true;
                        $scope.instalaciones[i].fases[j].fecha = new Date();
                        console.log('Fase iniciada: ', $scope.instalaciones[i].nombre,' -> ',$scope.instalaciones[i].fases[j].nombre);
                    }
                }
            }
        }
    }
    
    
    //Función para sobreescribir la variable 'hecha' de la fase
    $scope.finalizarFase = function(instalacion, fase){
        //Bucle para sustituir la instalación en la lista.
        for(var i=0; i<instalaciones.length; i++){  
            if(instalaciones[i].id==instalacion.id){
                //Bucle para poner la fase a hecha.
                for(var j=0; j<instalaciones[i].fases.length; j++){
                    if(instalaciones[i].fases[j].id == fase.id){
                        instalaciones[i].fases[j].hecha = true;
                        //instalaciones[i].fases[j].activa = false;
                        console.log('Fase finalizada: ', instalaciones[i].nombre, ' -> ', instalaciones[i].fases[j].nombre);
                    }
                }
            }
        }
    }
    
    $scope.guardarInst = function(instalacion){
        for(var i=0; i<instalaciones.length; i++){
            if(instalaciones[i].id==instalacion.id){
                instalaciones[i].nombre=instalacion.nombre;
                instalaciones[i].desc=instalacion.desc;
                instalaciones[i].prioridad=+instalacion.prioridad;
                console.log('Instalación modificada: ', instalaciones[i].id, '-',instalaciones[i].nombre);
            }
        }
    }
    
    $scope.guardarFaseInst = function(fase, instalacion){
        for(var i=0; i<instalaciones.length;i++){
            if(instalaciones[i].id==instalacion.id){
                for(var j=0; j<instalaciones[i].fases.length; j++){
                    if(instalaciones[i].fases[j].id==fase.id){
                        instalaciones[i].fases[j].duracion = fase.duracion;
                        console.log('Fase de Instalación modificada: ', instalaciones[i].nombre,'-',instalaciones[i].fases[j].nombre);
                    }
                }   
            }
        }
    }
    
    
    /*------------------------------------- Reset de Formularios -------------------------------------*/
    /*------------------------------------------------------------------------------------------------*/
    
    $scope.resetearFormIns = function(){
        $scope.x=undefined;
        $scope.mostrar = false;
        $scope.mostrarGrupo = false;
        $scope.mostrarFase = false;
        $scope.des = false;
        $scope.badNum = false;  
        $scope.error = false;
        $scope.grupoSel = "";
        $scope.fasesGrupoSel = "";
        $scope.myGrupo = {};
        $scope.formIns.$setPristine();
    }
    
    $scope.resetearFormGrupo = function() {
        $scope.formGrupo.$setPristine();
        $scope.noSel = false;
        $scope.mostrarGrupo = false;
        $scope.mostrarFase = false;
        $scope.grupo = undefined;
        grupoAux={};
        $scope.grupos = {};
        for (var j=0; j<$scope.fases.length; j++){
            $scope.fases[j].$selected = false;
        }
    }
    
    $scope.resetearFormFase = function() {
        $scope.formFase.$setPristine();
        $scope.mostrarFase = false;
        $scope.fases = {};
        $scope.f = undefined;
    }
});