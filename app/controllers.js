"use strict";   //Uso estricto de javaScript

//Directiva para imprimir documentos.
app.factory('printer', ['$rootScope', '$compile', '$http', '$timeout','$q', function ($rootScope, $compile, $http, $timeout, $q) {
        
        var printHtml = function (html) {
            var deferred = $q.defer();
            var hiddenFrame = $('<iframe style="display: none"></iframe>').appendTo('body')[0];
            hiddenFrame.contentWindow.printAndRemove = function() {
                hiddenFrame.contentWindow.print();
                $(hiddenFrame).remove();
            };
            
            var htmlContent = "<html>"+
                            '<body onload="printAndRemove();">' +
                                html +
                            '</body>'+
                        "</html>";
            var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
            doc.write(htmlContent);
            
            /* Para importar todos los css que ya tiene la aplicación (En este caso no sirve).
	            $('link[rel="stylesheet"]').each(function (i, ele) {
				var cssLink = doc.createElement('link');
            */
			var cssLink = doc.createElement('link');
			cssLink.href = 'print.css';
			cssLink.rel = 'stylesheet';
			cssLink.type = 'text/css';
			doc.getElementsByTagName("head")[0].appendChild(cssLink);
			
            //Creamos la variable que recoge el tamaño de la tabla.
			var el = angular.element($("#totalBody"));
			
			var cuerpoTabla = $("#divTabla").css("height");
			
			console.log('cuerpoTabla: ', cuerpoTabla);
			console.log('Contenido del documento: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all);
			
			console.log('HTML: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[0].offsetHeight); //2458			
			console.log('PRESUPUESTO: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[5].offsetHeight); //34
			console.log('Empresa - Cliente: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[8].offsetHeight); //222
			console.log('Nº Presupuesto: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[30].offsetHeight); //76
			//console.log('Footer: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[438].childNodes[0].offsetHeight); //290
			
			console.log('Tabla: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[35].offsetHeight); //750
			
			console.log('BODY - total: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[4].offsetHeight); //1069
			
			for(var i=0; i<el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all.length; i++){
				if(el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[i].id == "divFooter"){
					console.log('Footer: ', el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[i].offsetTop); //750
				}
			}
			
			// Como coge el número de páginas la función jquery - this.printTicketStore_.pageRange.getPageNumberSet().size
				
			//Recuperamos el total del tamaño del div del template.				
			var tamano = el.context.childNodes[1].childNodes[2].childNodes[25].contentDocument.all[35].offsetHeight;
			console.log('tamano: ', tamano);
			
			if(tamano > 550){
				var numero = parseInt(tamano / 550) ;	//Parte entera de la división
				console.log('numero (550): ', numero);
				var tamFinal = 100;
				if(numero == 1){
					tamFinal = 200;
				}else{
					console.log('tamFinal antes: ', tamFinal);
					numero = parseInt(tamano / 1045);
					console.log('numero (1045): ', numero);
					for(var i=0; i<numero; i++){
						tamFinal = tamFinal +100;
					}
					console.log('tamFinal final: ', tamFinal);
				}
				doc.body.style.height = tamFinal+"%";	
			}
			
			console.log('Cuerpo: ', doc.body.style.height);
			
            deferred.resolve();
            doc.close();
            return deferred.promise;
        };

        var openNewWindow = function (html) {
            var newWindow = window.open("impresionPresupuesto.html");
            newWindow.addEventListener('load', function(){ 
                $(newWindow.document.body).html(html);
            }, false);
        };

        var print = function (templateUrl, data) {
            $http.get(templateUrl).success(function(template){
                var printScope = $rootScope.$new()
                angular.extend(printScope, data);
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if(printScope.$$phase || $http.pendingRequests.length) {
                        $timeout(waitForRenderAndPrint);
                    } else {
                        //Replace printHtml with openNewWindow for debugging
                        printHtml(element.html());
                        //openNewWindow(element.html());
                        printScope.$destroy();
                    }
                };
                waitForRenderAndPrint();
            });
        };

        var printFromScope = function (templateUrl, scope) {
            $rootScope.isBeingPrinted = true;
            $http.get(templateUrl).success(function(template){
                var printScope = scope;
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if (printScope.$$phase || $http.pendingRequests.length) {
                        $timeout(waitForRenderAndPrint);
                    } else {
                       printHtml(element.html()).then(function() {
                           $rootScope.isBeingPrinted = false;
                       });

                    }
                };
                waitForRenderAndPrint();
            });
        };
        
        return {
            print: print,
            printFromScope: printFromScope
        }
}]);


app.controller('PresupuestosCntrl', PresupuestosCntrl);
function PresupuestosCntrl($scope, $rootScope, printer){
    
    $scope.tabla = [
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba texto de prueba texto de prueba texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba texto de prueba texto de prueba texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    }
                ];
    
    $scope.tabla2 = [
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba texto de prueba texto de prueba texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba texto de prueba texto de prueba texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    },
                    {
                        cantidad: 1,
                        coste: 10.05,
                        descripcion: "texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba texto de prueba",
                        horas_hombre: 1.25,
                        id: 219,
                        id_presupuesto: 7,
                        margen: 20,
                        mostrar: false,
                        nombre: "prueba",
                        pvp: 15.69,
                        referencia: "P2015091617471448",
                        total: 15.69,
                        visible: false,
                        visibleC: false,
                        visibleD: false
                    }
                ];
    
    //Función para imprimir la vista del presupuesto
	$scope.imprimir = function(num){		
		//Llamamos a la directiva que hemos creado.
		//El primer parámetro es la url al template que tenemos creado.
		//El segundo parámetro es un objeto que contiene los elementos que sustituiremos dentro del template.
		if(num == 1){
            var obj = {
                cliente: {
                    direccion: "C/ Cancamo Nº1",
                    email: "cancamo@gmail.com",
                    id: 3,
                    nombre: "Empresa C",
                    telefono: "916 43 61 52"
                },
                productos: $scope.tabla,
                impuesto: 21,
                iva: 355.48,
                presupuesto: {
                    base: 1692.75,
                    condiciones: "Según la LOPD se registrará el fichero de video y voz ante la ley...",
                    fecha: "2015-10-05",
                    financiado: false,
                    id: 7,
                    id_cliente: 3,
                    referencia: "PR2015100516522333",
                    terminado: false,
                    total: 2048.23
                }
            };
        }else{
            var obj = {
                cliente: {
                    direccion: "C/ Cancamo Nº1",
                    email: "cancamo@gmail.com",
                    id: 3,
                    nombre: "Empresa C",
                    telefono: "916 43 61 52"
                },
                productos: $scope.tabla2,
                impuesto: 21,
                iva: 355.48,
                presupuesto: {
                    base: 1692.75,
                    condiciones: "Según la LOPD se registrará el fichero de video y voz ante la ley...",
                    fecha: "2015-10-05",
                    financiado: false,
                    id: 7,
                    id_cliente: 3,
                    referencia: "PR2015100516522333",
                    terminado: false,
                    total: 2048.23
                }
            };
        }
        
        printer.print('template.html', obj);
	}
}